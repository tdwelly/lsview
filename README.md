# decals-image-list-tool
A simple webpage to generate image cutouts of the [Dark Energy Camera Legacy Survey](http://legacysurvey.org/) (DECaLS).

This tool is similar to the [SDSS Image List Tool](https://skyserver.sdss.org/dr13/en/tools/chart/listinfo.aspx). 

Original version of this tool can be found [here](https://yymao.github.io/decals-image-list-tool/)
[Code on GitHub](https://github.com/yymao/decals-image-list-tool)


Adapted by T.Dwelly to support target selection and survey planning activities for SDSS-V