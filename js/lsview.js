var _cols, _i_ra, _i_dec;
//, _has_mark,
var _delimiter, _numpix, _scale, _scale_unit, _layer;

var _output_img_temp = "<img class='picture ${zoom}' title='${title}' src='https://www.legacysurvey.org/viewer/jpeg-cutout/?ra=${ra}&dec=${dec}&${scale_unit}=${scale}&layer=${layer}&size=${numpix}${overlay}'>";
var _output_img_temp_hips = "<img class='picture ${zoom}' title='${title}' src='https://alasky.u-strasbg.fr/hips-image-services/hips2fits?hips=${hips_name}&width=${numpix}&height=${numpix}&fov=${size_deg}&projection=TAN&ra=${ra}&dec=${dec}&format=jpg'>";


//var _output_link_temp_sdss = "<a href='https://skyserver.sdss.org/dr16/en/tools/chart/navi.aspx?ra=${ra}&dec=${dec}' target='_blank'>${img}</a>";
var _output_link_temp_decals = "<a href='https://legacysurvey.org/viewer?ra=${ra}&dec=${dec}&layer=${layer}&mark=${ra},${dec}&zoom=18${overlay}' target='_blank'>${img}</a>";
var _output_link_temp_hips = "<a href='https://aladin.unistra.fr/AladinLite/?target=${hips_ra}${hips_dec}&fov=${hips_size_deg}&survey=${hips_name}' target='_blank'>${img}</a>";

var interval_fast = 25; // ms  - give the server a chance
var interval_slow = 250; // ms  - give the server a chance

/**
   From http://creativenotice.com/2013/07/regular-expression-in-array-indexof/
 * Regular Expresion IndexOf for Arrays
 * This little addition to the Array prototype will iterate over array
 * and return the index of the first element which matches the provided
 * regular expresion.
 * Note: This will not match on objects.
 * @param  {RegEx}   rx The regular expression to test with. E.g. /-ba/gim
 * @return {Numeric} -1 means not found
 */
if (typeof Array.prototype.reIndexOf === 'undefined') {
    Array.prototype.reIndexOf = function (rx) {
        for (var i in this) {
            if (this[i].toString().match(rx)) {
                return i;
            }
        }
        return -1;
    };
}

// selecting some rows in the text area will cause corresponding images to be highlighted.
function highlightImages() {
    let textarea = event.target;
    let first_line = textarea.value.substr(0, textarea.selectionStart).split("\n").length - 2;
    let last_line = textarea.value.substr(0, textarea.selectionEnd).split("\n").length - 2;
    // now highlight (via css) the set of images that correspond to this line range
    // remove all highlights

    const items = document.querySelectorAll('.picture');
    // Change the style of multiple elements with a loop
    for(i=0;i<items.length;i++){
        if (i>=first_line&&i<=last_line){
            items[i].classList.add('highlight');
        } else {
            items[i].classList.remove('highlight');
        };
    }
}

function parseHeader(line){
    // remove white space, then send to lower case, then remove any leading hash, then trim again.
    var line_orig = line.trim().replace('#','').trim();
    var line = line.trim().toLowerCase().replace('#','').trim();
    var has_header = 1;

    _delimiter = /\s+/;
    if (line.search("\\|") > -1) {
        _delimiter = "|";
    } else if (line.search(",") > -1) {
        _delimiter = ",";
    } else {
        _delimiter = /\s+/;
    }

    _cols = line.split(_delimiter).map(item => item.trim());
    var cols_orig = line_orig.split(_delimiter).map(item => item.trim());

    _i_ra = _cols.reIndexOf("^ra$");
    _i_dec = _cols.reIndexOf("^dec$");
    // try alternative patterns
    if (_i_ra == -1 || _i_dec == -1){ 
        _i_ra = _cols.reIndexOf("^ra_");
        _i_dec = _cols.reIndexOf("^dec_");
    }
    if (_i_ra == -1 || _i_dec == -1){ 
        _i_ra = _cols.reIndexOf("_ra$");
        _i_dec = _cols.reIndexOf("_dec$");
    }
    if (_i_ra == -1 || _i_dec == -1){ 
        _i_ra = _cols.reIndexOf("^ra");
        _i_dec = _cols.reIndexOf("^dec");
    }

    if ( _i_ra >= 0 && _i_dec >= 0 ) {
        $("#notes").html("<p style='color:green'>Coordinates for photometry viewers taken from these columns: "
                       + cols_orig[_i_ra] + ", " + cols_orig[_i_dec] + "</p>");
    }
    
    if (_i_ra == -1 || _i_dec == -1){ // no header
        if (_cols[_cols.length-1] == "true" || _cols[_cols.length-1] == "false"){
            //_has_mark = true;
            _cols.pop();
        }
        if (_cols.length == 2){
            _i_ra = 0;
            _i_dec = 1;
        }
        else if (_cols.length > 2){
            _i_ra = 1;
            _i_dec = 2;
        }
        else{
            //_has_mark = false;
            $("#list").html("<p style='color:red'>Error! First row cannot be blank.\nPlease make sure the first line is header, and it contains at least \"ra\" and \"dec\". \nOnly supports space/tab/pipe/comma-separated tables.</p>");
            return -1;
        }
        _cols = _cols.fill("");
        has_header = 0;
        $("#notes").html("<p style='color:orange'>Warning. Coordinates for cutouts are assumed to be in column numbers: "
                       + _i_ra + ", " + _i_dec + "</p>");
    }
    //else{ // has header
    //    if (_cols[_cols.length-1] == "marked"){
    //        //_has_mark = true;
    //        _cols.pop();
    //    }
    //}
    return has_header;
}

function getOverlay() {
    // const items = document.querySelector('[id^="overlay_"]');
    const items = document.getElementsByClassName('overlayCheckbox');
    // console.log(items);
    // console.log(items.length);
    var result = "";
    for (var i=0; i< items.length; i++){
        // console.log("Value = " +  items[i].value);
        if (items[i].checked ){
            // console.log("Is checked: " +  items[i].id);
            result = result + '&' + items[i].value;
        }
        // else
        // {
        //     // console.log("Not checked:" + items[i].id);
        // }
        
    }
    // console.log(result);
    return result;
}

function addImage(line) {
    var _hips_layers = [
        {'name': 'xmm', 'hips': 'ESAVO/P/XMM/EPIC-RGB'},
        {'name': 'csc', 'hips': 'cxc.harvard.edu/P/cda/hips/allsky/rgb'},
        {'name': '2mass', 'hips': 'CDS/P/2MASS/color'},
        {'name': 'vikingk', 'hips' : 'CDS/P/VISTA/VIKING/K'},
        {'name': 'uhsj', 'hips' : 'wfau.roe.ac.uk/P/UHSDR1/J'},
        {'name': 'ukidss_lasj', 'hips' : 'wfau.roe.ac.uk/P/UKIDSS/LAS/J'},
        {'name': 'ukidss_lask', 'hips' : 'wfau.roe.ac.uk/P/UKIDSS/LAS/K'},
        {'name': 'dss2', 'hips': 'CDS/P/DSS2/color'},
        {'name': 'nvss', 'hips': 'CDS/P/NVSS'},
        {'name': 'ps1', 'hips': 'CDS/P/PanSTARRS/DR1/color-i-r-g'},
     	{'name': 'galex_hips', 'hips': 'CDS/P/GALEXGR6/AIS/color'},
        {'name': 'sdssu', 'hips': 'CDS/P/SDSS9/u'},
        {'name': 'allwisew3', 'hips': 'CDS/P/allWISE/W3'},
        {'name': 'allwisew4', 'hips': 'CDS/P/allWISE/W4'},
        {'name': 'ztf', 'hips': 'CDS/P/ZTF/DR7/color'},
        {'name': 'skymapper', 'hips': 'CDS/P/Skymapper-color-IRG'},
        {'name': 'ls-dr10-color-hips', 'hips': 'alasky.cds.unistra.fr/DESI-legacy-surveys/DR10/CDS_P_DESI-Legacy-Surveys_DR10_color'},
        {'name': 'erosita/dr1/rate/021', 'hips': 'erosita/dr1/rate/021'},
        {'name': 'erosita/dr1/rate/022', 'hips': 'erosita/dr1/rate/022'},
        {'name': 'erosita/dr1/rate/023', 'hips': 'erosita/dr1/rate/023'},
        {'name': 'erosita/dr1/rate/rgb', 'hips': 'erosita/dr1/rate/rgb'},
        {'name': 'DES-DR2-HIPS', 'hips': 'CDS/P/DES-DR2/ColorIRG'},
        {'name': 'RACS-mid', 'hips': 'CSIRO/P/RACS/mid/I'},
        {'name': 'HST-UV', 'hips': 'CDS/P/HLA/wideUV'},
        {'name': 'HST-B', 'hips': 'CDS/P/HLA/B'},
        {'name': 'HST-V', 'hips': 'CDS/P/HLA/wideV'},
        {'name': 'HST-R', 'hips': 'CDS/P/HLA/R'},
        {'name': 'HST-I', 'hips': 'CDS/P/HLA/I'},
        {'name': 'HST-J', 'hips': 'CDS/P/HLA/J'},
    ];

    var _hips_name = "";
    for(var i=0; i < _hips_layers.length; i++){
        if (_hips_layers[i]['name'] == _layer) {
            _hips_name = _hips_layers[i]['hips'];
            break;
        }
    }

    var items, output; //, mark;
    items = line.trim().split(_delimiter).map(item => item.trim());
    //if (items.length < 2) {return;} // for empty lines
    
    //if (_has_mark) mark = (items.pop()=="true");
    if ( _hips_name != "" )  {
        output = _output_link_temp_hips.replace(/\${img}/g, _output_img_temp_hips);
    } else {
        //output = _output_img_temp;
        //if (!$("#enable_marking").prop("checked")){
        // if (_layer=='sdssco') output = _output_link_temp_sdss.replace(/\${img}/g, _output_img_temp);
        // else output = _output_link_temp_decals.replace(/\${img}/g, _output_img_temp);
        output = _output_link_temp_decals.replace(/\${img}/g, _output_img_temp);
        //  }
    }
    var _size_deg = Math.floor(_numpix) * _scale / 3600.0;

    var _overlay = getOverlay();
    
    output = output.replace(/\${ra}/g, items[_i_ra]);
    output = output.replace(/\${dec}/g, items[_i_dec]);
    output = output.replace(/\${hips_ra}/g, Number(items[_i_ra]).toFixed(7));
    var _dec_signed = (items[_i_dec]>= 0.0 ? '+'  : '' ) + Number(items[_i_dec]).toFixed(7); 
    output = output.replace(/\${hips_dec}/g, _dec_signed);
    output = output.replace("${scale_unit}", _scale_unit);
    output = output.replace("${scale}", _scale);
    output = output.replace(/\${numpix}/g, _numpix);
    output = output.replace(/\${size_deg}/g, _size_deg);
    output = output.replace(/\${hips_size_deg}/g, 4 * Number(_size_deg));
    output = output.replace(/\${layer}/g, _layer);
    output = output.replace(/\${hips_name}/g, _hips_name);
    output = output.replace("${title}", items.map(function(item, i, arr){return _cols[i] + " = " + item;}).join("\n"));
    output = output.replace(/\${overlay}/g, _overlay);

    if (document.querySelector('#toggle_zoom').checked) {
        output = output.replace(/\${zoom}/g, "zoom1p5");
    } else {
        output = output.replace(/\${zoom}/g, "");
    }
    // console.log("I will now append using: ", line); 
    $("#list").append(output);
}

function clear_and_run(){
    $("#list").empty();
    $("#notes").html("<p></p>");
    run();
}

function clear_list(){
    var textarea = document.querySelector('#input');
    // console.log(textarea);
    //console.log(textarea.value);
    textarea.value = "";
    $("#list").empty();
    $("#notes").html("<p></p>");
    //$("#list").addEventListener("select", highlightImages);
}

function run(){
    var new_layer = $("#layer").val();
    var new_numpix = $("#numpix").val();
    var new_scale = $("#scale").val();
    var new_scale_unit = $("#scale_unit").val();

    
    if (!_layer){
        if (!new_layer){
            new_layer = $("#default-layer").attr("value");
            $("#default-layer").prop("selected", true);
        }
        _layer = new_layer;
    }

    if (!_numpix) _numpix = new_numpix;
    if (!_scale) _scale = new_scale;
    if (!_scale_unit) _scale_unit = new_scale_unit;

    if (new_layer != _layer && _scale_unit == "zoom"){
        if (_layer != "sdssco" && new_layer == "sdssco"){
            new_scale = (parseInt(_scale) - 1).toString();
        }
        else if (_layer == "sdssco" && new_layer != "sdssco"){
            new_scale = (parseInt(_scale) + 1).toString();
        }
    }

    if (new_scale_unit != _scale_unit){
        if (new_scale_unit == "zoom"){
            new_scale = "14";
        }
        else {
            new_scale = "0.25";
        }
    }

    // if ($("#list").html()){
    //     $(".picture").each(function(){
    //         var url = $(this).attr("src");
    //        if (_layer != new_layer) url = url.replace("layer="+_layer, "layer="+new_layer);
    //        if (_scale_unit != new_scale_unit || _scale != new_scale) url = url.replace(_scale_unit+"="+_scale, new_scale_unit+"="+new_scale);
    //        $(this).attr("src", url);
    //    });
    //}
    //else
    {
        var lines = $("#input").val().split(/\n/);
        var has_header = parseHeader(lines[0]);
        if (has_header == -1) return;
      
        if (has_header > 0 ) lines.shift();
        if (lines.length <= 10 ) { var interval = interval_fast; } 
        else { var interval = interval_slow; }
        lines.forEach(function (el, index) {
            setTimeout(function () {
                addImage(el);
            }, index * interval);
        });

        $("#input").prop("readonly", false);
        $("#generate_output").removeClass("pure-button-primary");
        $("#generate_output").prop("disabled", true);
    }

    if (new_scale != _scale) $("#scale").val(new_scale);

    var refresh_flag = false;
    if (_layer != new_layer) {refresh_flag = true;} 

    _layer = new_layer;
    _numpix= new_numpix;
    _scale = new_scale;
    _scale_unit = new_scale_unit;

    if ( refresh_flag){
//        clear_and_run();
    }

}

// function new_run(){
//     //if (!$("#generate_output").prop("disabled") &&
//     //    !window.confirm("Are you sure you want to reload the images? You cannot recover the marks you made!")){
//     //  $("#enable_marking").prop("checked", !$("#enable_marking").prop("checked"));
//     //  return;
//     // }
//     $("#list").empty();
//     run();
// }

function output(){
    var out = "";
    out += _cols.join("\t");
    out += "\tmarked\n";
    if (out.trim() == "marked") out = "";
    out += $(".picture").map(function(){
        return $(this).attr("title").split(/\n/).map(function(item){
            return item.substring(item.indexOf(" = ")+3);
        }).join("\t") + "\t" + $(this).hasClass("marked");
    }).get().join("\n");
    $("#input").val(out);
    $("#generate_output").prop("disabled", true);
}

$("form").submit(function(event){event.preventDefault();});
$("#generate_output").click(output);
//$("#layer").change(run);
//$("#numpix").change(run);
//$("#scale").change(run);
//$("#scale_unit").change(run);
//$("#input").change(function(){
//    $("#list").empty();
//    run();
//});


var el = document.getElementById('input');

//console.log("Adding event listener to:");
//console.log(el);
//console.log(typeof el);
el.addEventListener('select', highlightImages);
el.addEventListener('click', highlightImages);
